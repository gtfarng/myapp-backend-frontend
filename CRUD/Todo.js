let express = require('express')
let bodyParser = require('body-parser')
let router = express.Router()
let cors = require('cors')
let app = express()

app.use(cors())
// all of our routes will be prefixed with /api

app.use('/api', bodyParser.json(), router) //[use json]
app.use('/api', bodyParser.urlencoded({ extended: false }), router)

let todos = [{ 'id': 0, 'activity': 'Programing', 'location': 'Company', 'Time': '2562-01-01T12:00' },
             { 'id': 1, 'activity': 'Installing ', 'location': 'Outdoor ', 'Time': '2562-02-01T17:00' }
];

let todoIndex = 2;

router.route('/todos')
    .get((req, res) => res.json(todos))
    .post((req, res) => {
        var todo = {};
        todo.id = todoIndex++;
        todo.activity = req.body.activity
        todo.location = req.body.location
        todo.Time = req.body.Time
        todos.push(todo);
        res.json({ message: 'todo created!' })
    })

router.route('/todos/:todo_id')
    .get((req, res) => res.json(todos[req.params.todo_id])) 
    .put((req, res) => {
        var id = req.params.todo_id
        todos[id].id = req.body.id
        todos[id].activity = req.body.activity
        todos[id].location = req.body.location
        todos[id].Time = req.body.Time
        res.json({ message: 'todo updated!' + req.params.todo_id });
    })

    .delete((req, res) => { 
        delete todos[req.params.todo_id]
        res.json({ message: 'todo deleted: ' + req.params.todo_id });
    })



app.use("*", (req, res) => res.status(404).send('404 Not found'))
app.listen(8000, () => console.log("Server is running"))





