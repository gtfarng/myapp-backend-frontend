import React, { Component } from 'react'
import './App.css'

class Count extends Component {
    state = {number: 0}
    add = () => this.setState({number: this.state.number + 1})
    minus = () => this.setState({number: this.state.number - 1})
 
    render() {
        return (
                <div style={{margin: '20px'}}>
                   <h1> Counter: {this.state.number} </h1> <br/>
                    <button onClick={this.add} > Up</button>
                    <button onClick={this.minus}> Down</button>
                </div>
        );
    }
 }
 
 export default Count