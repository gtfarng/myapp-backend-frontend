import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import Count from './Count' 

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" width='300px' />
          <h1>My-App </h1>
        </header>

        <div>
              <Count />
        </div>




      </div>
    );
  }
}

export default App;
