import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import Count from './Count'
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'

export const add = () => ({ type: 'ADD' })
export const minus = () => ({ type: 'MINUS' })
export const numberReducer = (state = 0, action) => {
  switch (action.type) {
    case 'ADD':
      return state + 1
    case 'MINUS':
      return state - 1
    default:
      return state
  }
}
export const rootReducer = combineReducers({ number: numberReducer })
export const store = createStore(rootReducer)

store.dispatch(add());
console.log('getState: ', store.getState());


export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" width='300px' />
          <h1>My-App </h1>
        </header>

        <div>
          <Provider store={store}><Count />

          </Provider>
        </div>




      </div>
    );
  }
}



