import React, { Component } from 'react'
import './App.css'
import { add, minus, store } from './App'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return { number: state.number }
}


class Count extends Component {
    state = { number: 0 }
    add = () => this.setState({ number: this.state.number + 1 })
    minus = () => this.setState({ number: this.state.number - 1 })

    render() {
        return (
            <div>
                {/*   non-redux
                <div style={{ margin: '20px' }}>
                    <h1> Counter: {this.state.number} </h1> <br />
                    <button onClick={this.add} > Up</button>
                    <button onClick={this.minus}> Down</button>
                </div>
        */} 
            { // redux 
            }
                <div style={{ margin: '20px' }}>
                  <h1>  Counter: {this.props.number}</h1> <br />
                    <button onClick={() => store.dispatch(add())}>Up</button>
                    <button onClick={() => store.dispatch(minus())}>Down</button>
                </div>


            </div>
        );
    }
}

export default connect(mapStateToProps)(Count);
