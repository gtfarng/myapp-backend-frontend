import React, { Component } from 'react'
import './App.css'
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'
import Count from './API/Count'


export const add = () => ({ type: 'ADD' })
export const minus = () => ({ type: 'MINUS' })
export const add2 = (number) => ({ type: 'ADD2', number })
export const minus2 = (number) => ({ type: 'MINUS2', number })
export const set = (number) => ({ type: 'SET', number })
export const reset = () => ({ type: 'RESET' })

export const numberReducer = (state = 0, action) => {
    switch (action.type) {
        case 'ADD':
            return state + 1
        case 'ADD2':
            return state + action.number
        case 'MINUS':
            return state - 1
        case 'MINUS2':
            return state - action.number
        case 'SET':
            return action.number
        case 'RESET':
            return 0
        default:
            return state
    }
}

export const rootReducer = combineReducers({ number: numberReducer })
export const store = createStore(rootReducer, applyMiddleware(logger, thunk))

export default class App extends Component {
    render() {
        return (
            <div className="App" >
                <header className="App-header">

                    <br />  <h1>My-App ( React-Redux-Counter ) </h1><br />
                </header>

                <div>
                    <Provider store={store}>
                        <Count />
                    </Provider>


                </div>
            </div>
        );
    }
}


