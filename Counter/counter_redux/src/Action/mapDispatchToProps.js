import {add, minus, reset} from "../App";

const mapDispatchToProps = (dispatch) => {
    return {
        add: () => dispatch(add()),
        minus: () => dispatch(minus()),
        reset: () => dispatch(reset()),

    }
}