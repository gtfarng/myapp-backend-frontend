import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import Count from './Count'
import { Provider } from 'react-redux'
import {createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'

export const add = () => ({ type: 'ADD' })
export const add2 = (number) => ({ type: 'ADD2', number })
export const reset = () => ({ type: 'RESET' })
export const minus = () => ({ type: 'MINUS' })
export const minus2 = (number) => ({ type: 'MINUS2', number })
export const numberReducer = (state = 0, action) => {
  switch (action.type) {
    case 'ADD':
      return state + 1
    case 'ADD2':
      return state + action.number
    case 'RESET':
      return 0
    case 'MINUS':
      return state - 1
    case 'MINUS2':
      return state - action.number
    default:
      return state
  }
}
export const rootReducer = combineReducers({ number: numberReducer })
export const store = createStore(rootReducer, applyMiddleware(logger))

store.dispatch(add());
console.log('getState: ', store.getState());


export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" width='300px' />
          <h1>My-App </h1>
        </header>

        <div>
          <Provider store={store}>
          <Count />

          </Provider>
        </div>
      </div>
    );
  }
}



