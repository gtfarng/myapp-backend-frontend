import React, { Component } from 'react'
import './App.css'
import { add, add2, minus, reset, minus2, store } from './App'
import { connect } from 'react-redux'

const mapStateToProps = (state) => {
    return { number: state.number }
}

const mapDispatchToProps = (dispatch) => {
    return {
        add: () => dispatch(add()),
        minus: () => dispatch(minus())
    }
}

class Count extends Component {
    state = { number: 0 }
    add = () => this.setState({ number: this.state.number + 1 })
    minus = () => this.setState({ number: this.state.number - 1 })

    render() {
        return (
            <div>
                {/*   non-redux
                <div style={{ margin: '20px' }}>
                    <h1> Counter: {this.state.number} </h1> <br />
                    <button onClick={this.add} > Up</button>
                    <button onClick={this.minus}> Down</button>
                </div>
        */}
                { // redux 
                }
                <div style={{ margin: '20px' }}>
                    <h1>  Counter: {this.props.number}</h1> <br />
                    <button onClick={() => store.dispatch(add())}>Up</button>
                    <button onClick={() => store.dispatch(minus())}>Down</button>
                    <button onClick={() => store.dispatch(reset())}>Reset</button>
                    <button onClick={() => store.dispatch(add2(2))}>Up+2</button>
                    <button onClick={() => store.dispatch(minus2(2))}>Up-2</button>
                </div>


            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Count)
