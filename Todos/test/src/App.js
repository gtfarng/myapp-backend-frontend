import React, { Component } from 'react';
import './App.css';
import Todo from './containers/Todo'

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h1>My-App</h1>
        </header>
        <Todo />

      </div>
    );
  }
}


