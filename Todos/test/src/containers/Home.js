import React, { Component } from 'react';
import { filtering } from "../actions/index";
import { connect } from 'react-redux';
import '../App.css'

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = {
            id: "5735512002",
            name: "Jatupat",
            surname: "Pannoi"
        }
    }
    handleChange = (e) => {
        const { name, value } = e.target;
        console.log(name);
        this.setState({ [name]: value })
        this.setState({ [name]: value });

    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.filtering(this.state.id);
    }

    render() {
        const { WhenGrowup } = this.props;
        console.log(this.props);
        console.log('state', this.state);
        return (
            <div style={{ margin: '50px' }} align="center">
                <h1>  Todo-App ( ReactApp-Redux )  </h1>
                <p>
                    ID : {this.state.id} <br />
                    Name : {this.state.name} <br />
                    Surname : {this.state.surname}
                </p>
                <ul>
                    {
                        WhenGrowup.map(value => {
                            return <List key={value.id} id={value.id} activity={value.activity} location={value.location} time={value.time} />
                        })
                    }

                </ul>
                <form onSubmit={this.handleSubmit}>
                    <input type="text" name="id" onChange={this.handleChange} />
                    <button type="submit">Submit</button>
                </form>
            </div>
        )
    }
}

const List = (props) => {
    return (
        
        <li >
           ID: {props.id} {props.activity} at {props.location} in {props.time} 
        </li>
        
    )
}

const mapStateToProps = ({ WhenGrowup }) => {
    return {
        WhenGrowup
    }
}

export default connect(mapStateToProps, { filtering })(Home);
