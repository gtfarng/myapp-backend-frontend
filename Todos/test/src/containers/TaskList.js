import React, {Component} from 'react'
import '../App.css'

export default class TaskList extends Component {
   render() {
       if ( this.props.tasks )
           return (
          
 			
           	<ul > 
           	{
                   this.props.tasks.map((item) => 
                   (
                       <li className="tab" key={item.id}>{item.task} at {item.place} in {item.daytime}</li>
                   ))
               }
           </ul>


         )
   }
}

