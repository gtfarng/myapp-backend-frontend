import React, { Component } from 'react';
import { filtering } from "../actions/index";
import { connect } from 'react-redux';
import { add } from '../actions/actionCreator'
import InputTask from './InputTask'
import TaskList from './TaskList'
import '../App.css'

//export const rootReducer = combineReducers({number: numberReducer })
//export const store = createStore(rootReducer, applyMiddleware(logger,thunk))

class Todo extends Component {
    constructor(props) {
        super(props)
        this.state =
            {
                tasks: [{ id: '2', task: 'Mantainance', place: 'Server', date: '2562-03-16', daytime: '2562-03-16T17:00' }],
                id: 3
            }



    }
    onChangeTodo = (e) => {
        this.setState({
            activity: e.target.value
        })
    }
    handleChange = (e) => {
        const { name, value } = e.target;
        console.log(name);
        this.setState({ [name]: value })


    }
    handleSubmit = (e) => {
        e.preventDefault();
        this.props.filtering(this.state.id);
    }

    addTask = (task, place, date, daytime) => {
        this.setState({
            tasks: [...this.state.tasks, { id: this.state.id, task, place, date, daytime }],
            id: this.state.id + 1
        })
    }



    render() {
        const { WhenGrowup } = this.props;
        console.log(this.props);
        console.log('state', this.state);
        return (
            <div style={{ margin: '50px' }} align="center">
                <h1>  Todo-App ( ReactApp-Redux )  </h1>

                <ul>
                    {
                        WhenGrowup.map(value => {
                            return <List key={value.id} id={value.id} activity={value.activity} location={value.location} time={value.time} />
                        })
                    }
                   
                </ul>
                <TaskList tasks={this.state.tasks} />

                <h5>

                    <InputTask addTask={this.addTask} id={this.state.id} />
{/*
                    <br /><br /><br />
                    <input onChange={this.onChangeTodoText} name="id" value={this.state.id} type="hidden" />
                    <input onChange={this.onChangeTodo} name="activity" value={this.state.activity} type="text" placeholder="Activity" />
                    <a className="tab">at</a><a className="tab"></a>
                    <input onChange={this.onChangeTodoText} name="location" value={this.state.location} type="text" placeholder="Location" />
                    <a className="tab">In</a><a className="tab"> </a>
                    <input onChange={this.onChangeTodoText} name="time" value={this.state.time} type="datetime-local" placeholder="Activity" /><br />

                    <button type="button" onClick={() => this.props.addTask(this.state.activity, this.state.location, this.state.time)} >Add</button>
*/             }

                </h5>



            </div>
        )
    }
}

const List = (props) => {
    return (
        <li>  {props.activity} at {props.location} in {props.time}  </li>

    )
}

const mapStateToProps = ({ WhenGrowup }) => {
    return {
        WhenGrowup
    }
}

export default connect(mapStateToProps, { filtering })(Todo);
