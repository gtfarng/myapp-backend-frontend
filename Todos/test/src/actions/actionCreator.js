import { ADD } from './actionTypes'

let TodoId = 2

export const add = (text) => ({
    type: ADD,
    id: TodoId++,
    text
})

