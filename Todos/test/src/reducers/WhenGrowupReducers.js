import { FILLTERING } from "../actions/index";

const initialState = [
    { id: 0, activity: "Programing", location: "Company", time: "2562-01-01T12:00" },
    { id: 1, activity: "Installing", location: "Outdoor ", time: "2562-02-01T17:00" },

]

export default function (state = initialState, action) {

    switch (action.type) {
        case
            FILLTERING:
            return state.filter(value => value.id !== parseInt(action.payload))
        default:
            return state = initialState;
    }
    return initialState;
}