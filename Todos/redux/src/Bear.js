import React, { Component } from 'react'
import { getBears } from './App'
import { connect } from 'react-redux'

const mapStateToProps = ({ bears }) => {
    return { bears }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getBears: () => dispatch(getBears())
    }
}

class Bear extends Component {

    componentDidMount() {
        console.log('props', this.props)
        this.props.getBears()
    }

    renderBears = () => {
        if (this.props.bears) {
            return this.props.bears.map((bear, index) => {
                console.log(bear.name)
                return (
                <div>
                   
                <li key={index}> {bear.name} : {bear.weight} </li></div>
                )
            })
        }
    }

    render() {
        return (
            <div>
            <div style={{ margin: '50px' }}>
                <h1>Render Bear</h1>
               <h3><small> Name : weight</small></h3>
                <ul>
                    {this.renderBears()}
                </ul>
            </div>
            </div>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Bear);

