import React, { Component } from 'react'
import { getTodos } from './App'
import { connect } from 'react-redux'

class Todo extends Component {

    componentDidMount() {
        console.log('props', this.props)
        this.props.getTodos()
    }

    renderTodos = () => {
        console.log('Todo xx: ' + this.props.todos)
        if (this.props.todos) {
            return this.props.todos.map((todo, index) => {
                console.log('kk: ',todo.activity)
                return (
                    <li key={index}> 
                        {todo.activity} : {todo.location}: {todo.Time} 
                    </li>
                )
            })
        }
    }

    render() {
        return (
            <div>
            <div style={{ margin: '50px' }}>
                <h1>Render Todo</h1>
               <h3><small> Activity : Location : Time</small></h3>
                <ul>
                    {this.renderTodos()}
                </ul>
            </div>
            </div>
        );
    }
}

const mapStateToProps = ({ todos }) => {
    console.log('msp: ', todos)
    return { todos }
}

const mapDispatchToProps = (dispatch) => {
    return {
        getTodos: () => dispatch(getTodos())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Todo);

