import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import axios from 'axios'
import thunk from 'redux-thunk'
import Todo from './Todo'

export const getTodosSuccess = todos => ({
  type: 'GET_TODOS_SUCCESS',  todos
});

export const getTodosFailed = () => ({ type: 'GET_TODOS_FAILED' });

export const getTodos = () => async (dispatch) => {
  try {
    console.log('get todo new')
    const response = await axios.get(`http://localhost:8000/api/todos`)
    const responseBody = await response.data;
    console.log('response: ', responseBody)
    dispatch(getTodosSuccess(responseBody));
  } catch (error) {
    console.error(error);
    dispatch(getTodosFailed());
  }
}

export const todoReducer = (state = 0, action) => {
  switch (action.type) {
    case 'GET_TODOS_SUCCESS':
      console.log('action x: ', action.todos)
      return action.todos
    case 'GET_TODOS_FAILED':
      console.log('action: Failed')
      return action.todos
    default:
      return state
  }
}

export const rootReducer = combineReducers({ todos: todoReducer })
export const store = createStore(rootReducer, applyMiddleware(logger, thunk))

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />

        </header>

        <div>
          <Provider store={store}>
            <Todo />
          </Provider>
        </div>

      </div>
    )
  }
}


