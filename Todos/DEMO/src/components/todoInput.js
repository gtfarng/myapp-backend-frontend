import React, { Component } from 'react';
import { addTodo } from '../actions';
import { connect } from 'react-redux';
import '../App.css'

class TodoInput extends Component {
    constructor() {
        super();
        this.state = {
            inputVal: '',
            location:'',
            disable: true
        }
    }

    handleInput = (e) => {
        this.setState({
            inputVal: e.target.value
        }, () => {
            !this.state.inputVal === '' ? this.setState({
                disable: false
            }) :
                this.setState({
                    disable: true
                })
        })
    }

    handleInputs = (e) => {
        this.setState({
            location: e.target.value
        }, () => {
            !this.state.location === '' ? this.setState({
                disable: false
            }) :
                this.setState({
                    disable: true
                })
        })
    }

    
    handleSubmit() {

        let myDate = new Date();
        let day = myDate.getDate();
        let month = myDate.getMonth()+1;
        let year = myDate.getFullYear();
        let hour = myDate.getHours();
        let minute = myDate.getMinutes();
        let sec = myDate.getSeconds();
        if (day < 10) {
            day = '0' + day;
        }
        if (month < 10) {
            month = '0' + month;
        }
        let dateString = day + '/' + month + '/' + year + ' & ' + hour + ':' + minute + ':' + sec;
        this.props.addTodo(this.state.inputVal, dateString,this.state.location)
    }

    render() {
        return (
            <div align="center">
                <input type="text" name="itemInput" placeholder="Enter you todos here..."
                    value={this.state.inputVal}
                    onChange={this.handleInput.bind(this)}
                   
                /><a  className="tab"></a>

                <input type="text" name="itemInput" placeholder="Enter you location here..."
                    value={this.state.location}
                    onChange={this.handleInputs.bind(this)}
                    
                />
                
                <br /><br />

                <button
                    className="btn btn-primary ml-2"
                    onClick={this.handleSubmit.bind(this)}
                    
                >SUBMIT</button><br /><br />
            </div>
        )
    }
}


const mapDispatchToProps = dispatch => ({
    
    addTodo: (text, date,location) => dispatch(addTodo(text, date,location)),
    
});

export default connect(null, mapDispatchToProps)(TodoInput);