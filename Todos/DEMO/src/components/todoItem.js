import React, { Component } from 'react';
import { connect } from 'react-redux';
import { deleteTodo } from '../actions';
import '../App.css'

class TodoItem extends Component {
    constructor(props) {
        super(props);
    }

    deleteTodo = (id) => {
        this.props.deleteTodo(id);

    }

    render() {
        return (
            <li className="list-group-item align-middle">
                <div onClick={this.props.onClick}
                    style={this.props.completed ? { textDecoration: 'line-through' } : { textDecoration: 'none' }}><strong>Activity :</strong> <a className="tab">{this.props.todo.activity}</a>

                    <br /> <br /><p><strong>Location :</strong> <a className="tab">{this.props.todo.location}</a></p>
                    <p><strong>Date And Time :</strong> <a className="tab">{this.props.date}</a></p>
                
                <br />
                </div>
                <button className="btn btn-danger d-block" onClick={() => this.deleteTodo(this.props.todo.id)}>DELETE</button>
            </li>
        )
    }
}

const mapDispatchToProps = dispatch => ({
    deleteTodo: (id) => dispatch(deleteTodo(id))
})

export default connect(null, mapDispatchToProps)(TodoItem);