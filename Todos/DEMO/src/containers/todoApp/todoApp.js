import React, { Component } from 'react';
import TodoInput from '../../components/todoInput';
import TodoList from '../../components/todoList';
import '../../App.css'

export default class TodoApp extends Component {
    render(){
        return(
            <div className="container">
                <div>
               <div align="center" className="App-header">
                <br/><h1>Todo-App </h1>
                <h3>(React-Redux)</h3><br/>
                </div>
                    <TodoList /><br/><br/>
                    <TodoInput />
                
                </div>
            </div>
        )
    }
}

