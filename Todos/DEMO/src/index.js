import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import TodoApp from './containers/todoApp/todoApp';
import {createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux';
import rootReducer from './reducers';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk'
import logger from 'redux-logger'


const store = createStore(rootReducer,composeWithDevTools(),applyMiddleware(logger,thunk));


ReactDOM.render(
    <Provider store={store}>
        <TodoApp />
    </Provider>,
document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
