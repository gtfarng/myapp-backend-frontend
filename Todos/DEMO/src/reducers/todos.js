import { ADD_TODO, TOGGLE_TODO, DELETE_TODO } from "../constants/constants";
import intialData from '../intialData/intialData'

const intialState=intialData.task
const todos = (state=[],action) => {
    switch(action.type){
        case ADD_TODO:
            return [
                ...state,
                {
                    id:action.id,
                    activity:action.activity,
                    location:action.location,
                    completed:false,
                    date:action.date
                }
            ];
        case TOGGLE_TODO:
            return state.map(todo =>
               (todo.id===action.id) 
               ? {...todo, completed: !todo.completed } 
               : todo
            )
        case DELETE_TODO:
            return state.filter(todo => todo.id !== action.id)
        default:
            return state;
    }
}

export default todos;