import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import Todo from './Todo'
import { Provider } from 'react-redux'
import { createStore, combineReducers } from 'redux'

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1>Todo</h1>
        </header>

        <div>

        </div>
      </div>
    );
  }
}


