import React, { Component } from 'react'
import logo from './logo.svg'
import './App.css'
import { Provider } from 'react-redux'
import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import axios from 'axios'
import thunk from 'redux-thunk'
import Bear from './Bear'

export const getBearsSuccess = bears => ({
  type: 'GET_BEARS_SUCCESS',
  bears
});
export const getBearsFailed = () => ({ type: 'GET_BEARS_FAILED' });

export const getBears = () => async (dispatch) => {
  try {
    console.log('get bear new')
    const response = await axios.get(`http://localhost:8000/api/bears`)
    const responseBody = await response.data;
    console.log('response: ', responseBody)
    dispatch(getBearsSuccess(responseBody));
  } catch (error) {
    console.error(error);
    dispatch(getBearsFailed());
  }
}

export const bearReducer = (state = 0, action) => {
  switch (action.type) {
    case 'GET_BEARS_SUCCESS':
      console.log('action: ', action.bears)
      return action.bears
    case 'GET_BEARS_FAILED':
      console.log('action: Failed')
      return action.bears
    default:
      return state
  }
}

export const rootReducer = combineReducers({ bears: bearReducer })
export const store = createStore(rootReducer, applyMiddleware(logger, thunk))

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />

        </header>

        <div>
          <Provider store={store}>
            <Bear />
          </Provider>
        </div>

      </div>
    )
  }
}


